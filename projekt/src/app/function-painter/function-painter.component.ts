import { Component, OnInit } from '@angular/core';
import { parse } from 'url';
import { ChartsModule, Color } from 'ng2-charts';

@Component({
  selector: 'app-function-painter',
  templateUrl: './function-painter.component.html',
  styleUrls: ['./function-painter.component.css']
})
export class FunctionPainterComponent implements OnInit {
  argsString : string;
  arguments : string;
  datasets : any[];
  chartType : string = 'line'
  colors : Array<Color> = [];
  labels : string[] = []
  values : number[];

  isVisible : boolean;

  options : any;

  constructor() { 
    this.argsString = '';
    this.arguments = '';
    this.values = [0, 1];
    this.datasets = [{data : this.values, label : "Wykres"}];
    this.labels = ["0", "1"];
    this.options = { responsive: false };
    this.isVisible = true;
  }

  ngOnInit() {
  }

  onClick()
  {
    this.isVisible = false;
    this.calculateValues()
  }

  parseElements(input : string) : number[]
  {
    return input.split(',').map(function(item)
        {
          return parseFloat(item);
        });
  }

  calculateValues() : void
  {
    var values = this.parseElements(this.argsString);

    var newLabels = [];
    for(var index = 0; index < values.length; index++)
    {
      newLabels.push(index.toString());
    }
    this.labels = newLabels;
    this.values = values;
    this.datasets = [{data : values, label : "Wykres"}];
    setTimeout(()=>{this.isVisible = true}, 0);
  }
}
